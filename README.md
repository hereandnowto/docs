# Messages to repeat

Which problem of the user is this app solving?

1. A local app for blind dating, **safer from scammers and abusers**.

## Features

### Buy

Buy android/iOS app with 20 dollars refundable. In case you abuse our Terms Of Use, we have the right to sue the bank account holder.

### Refund

Submit a request for a refund by giving us a detailed honest and polite review of our app through [Canny](https://canny.io)

### Setup

Set gender (Male, Female, Non-binary), choose one or more of what you are looking for (Male, Female, Non-binary) and choose what interests you :

a. friends
b. sex
c. relationship
d. marriage

### Location sharing

1. start sharing your location anonymously (and choose the precision or make it exact enough so that someone in a bar can approach you by both of you getting dressed with the right color) 
2. stop sharing (the system stores your choice and that the whole area got a color, not your location and nothing else)

Should we limit places for better safety to places bought by merchants?

### Navigate on a map

1. Search on a live map
2. Pick a location shared and inform that person that you'll be there in less than 30 minutes

### Postponed features

1. (Search on a map that aggregates anonymous data per week per hour [In a later version, review whether we should replace this with per month and less precision])

### Dropped features

Partner with KYC providers [1](https://www.idnow.io) or [2](https://webid-solutions.de/?lang=en)

## Deliverable 1 is to come up with Viable Product requirements

1. Survey like 5-10 daily statements at 1-2 twitter influencers:

    1. e.g. Like if you would pay for a location-based blind dating app with no profiles to meet heterosexual people interested in casual sex. Join newsletter [hereandnow.to](https://hereandnow.to/casualsex/city) to learn when we go live.
    2. e.g. Like if you would pay for a location-based blind dating app with no profiles to meet heterosexual people interested in a relationship. Join newsletter [hereandnow.to](https://hereandnow.to/relationship/city) to learn when we go live.
    3. e.g. Like if you would pay for a location-based blind dating app with no profiles to meet heterosexual people interested in marriage. Join newsletter [hereandnow.to](https://hereandnow.to/marriage/city) to learn when we go live.

2. Website
3. Form to gather interests per city
4. Newsletter and campaign
5. Donation-based crowdfunding to individuals (should we use [this](http://fundly.com/how-to-fundraise) to create our website?)

### Posts

create a video for some of the 10 statements the influencers will post to help the users understand what we talk about and avoid getting confused with current dating apps and websites.

https://intellifluence.com
https://webfluential.com
https://influencity.com

## Deliverable 2 is to come up with Minimum Product specs

1. Design preferrably in figma with description of user flow and usage metrics
2. Screenshots and video

## Deliverable 3

1. Android app developed and published with error tracking
2. iOS app and published with error tracking
